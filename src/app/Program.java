package app;

import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import domain.Aluno;

public class Program {
	
	public static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("exemplo-jpa");
	public static EntityManager entityManager = entityManagerFactory.createEntityManager();
	
	public static void main(String[] args) {	
		Scanner scanner = new Scanner(System.in);
		int choose;
		int id;
		String name;
		String cpf;
		
		do {
			menu();
			choose = scanner.nextInt();
			
			switch (choose) {
				case 1:
					listAlunos();
					break;
					
				case 2:
					System.out.print("\nDigite o ID do Aluno: ");
					id = scanner.nextInt();
					System.out.println("\n" + findAlunoById(id) + "\n");
					break;
					
				case 3:
					System.out.print("\nDigite o NOME do Aluno: ");
					name = scanner.next();
					do {
						System.out.print("Digite o CPF do Aluno: ");
						cpf = scanner.next();
						
						if (cpf.length() < 11) {
							System.out.println("\n\nCPF INVÁLIDO\n");
						}
						
					} while (cpf.length() < 11);
					
					addAluno(name, cpf);
					
					break;
				
				case 4:
					do {
						System.out.print("\nDigite o ID do Aluno a ser atualizado: ");
						id = scanner.nextInt();
						if (entityManager.find(Aluno.class, id) == null) {
							System.out.println("\n\nALUNO NÃO ENCONTRADO\\n");
						}
					} while (entityManager.find(Aluno.class, id) == null);
					
					System.out.print("Digite o NOME do Aluno: ");
					name = scanner.next();
					do {
						System.out.print("Digite o CPF do Aluno: ");
						cpf = scanner.next();
						
						if (cpf.length() < 11) {
							System.out.println("\n\nCPF INVÁLIDO\n");
						}
						
					} while (cpf.length() < 11);
					
					updateAluno(id, name, cpf);
					break;
				
				case 5:
					do {
						System.out.print("\nDigite o ID do Aluno a ser removido: ");
						id = scanner.nextInt();
						if (entityManager.find(Aluno.class, id) == null) {
							System.out.println("\n\nALUNO NÃO ENCONTRADO\\n");
						}
						
					} while (entityManager.find(Aluno.class, id) == null);
					
					removeAluno(id);
					break;
			}
	
			
		} while (choose != 0);
		
		entityManager.close();
		entityManagerFactory.close();
		scanner.close();
		
		
	}
	
	public static void menu() {
		System.out.print("1 - Listar pessoas cadastradas\n" +
		"2 - Buscar pessoa pelo id\n" + 
		"3 - Cadastrar pessoa\n" +
		"4 - Atualizar pessoa\n" +
		"5 - Remover uma pessoa\n" +
		"0 - Sair\n\n" +
		"Escolha: ");

	}
	
	public static void listAlunos () {
		String jpql = "SELECT p FROM Aluno p";
		List<Aluno> list = entityManager.createQuery(jpql, Aluno.class).getResultList();
		for (Aluno aluno: list) {
			System.out.println(aluno);
		}
		System.out.println();
		
	}
	
	public static Aluno findAlunoById(int id) {
		Aluno alunoFound = entityManager.find(Aluno.class, id);
		return alunoFound;

	}
	
	public static void addAluno (String nome, String cpf) {
		Aluno aluno = new Aluno(null, nome, cpf);
		entityManager.getTransaction().begin();
		entityManager.persist(aluno);
		entityManager.getTransaction().commit();
		
		System.out.println("\n\nPessoa cadastrada com sucesso!\n");
	}
	
	public static void updateAluno (int id, String nome, String cpf) {
		Aluno alunoFound = entityManager.find(Aluno.class, id);
		alunoFound.setNome(nome);
		alunoFound.setCpf(cpf);
		entityManager.getTransaction().begin();
		entityManager.persist(alunoFound);
		entityManager.getTransaction().commit();
		
	}
	
	public static void removeAluno (int id) {
		Aluno aluno = entityManager.find(Aluno.class, id);
		entityManager.getTransaction().begin();
		entityManager.remove(aluno);
		entityManager.getTransaction().commit();
	}

}
